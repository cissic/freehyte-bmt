function h = surfScatteredData(x,y,z)

% SURFSCATTEREDDATA creates a 3D-surf-type figure of the scattered data
% basing of Delaunay triangulation of the given (x,y) nodes.

% INPUT:
% x,y,z - mesh points and function values given as column vectors
% OUTPUT:
% h - plot handle

load('dBMT_StructDef','p','t','e') ; 

if (isscalar(p) && p == 0)
    % we've got regular (rectangular) mesh of the simply-connected domain
    tri = delaunay(x,y);
    % h = trisurf(tri, x, y, z, 'EdgeColor','none', 'FaceColor','interp');
else
    % If the mesh is irregular we need to check whether there are any holes 
    % inside the domain.    
    % Since hole-detection is already performed in dBMT_BC1 ->dBMT_BC1_OpeningFcn()
    % here we just load what was saved there.
    load('irreg_mesh', 'e', 'ePolygon','polygonNo', 'index_of_external_polygon') ;
    % e         - stores consecutive edges of the domain
    % ePolygon  - ePolygon(1,ii) stores the number of the polygon to which
    %              the edge under ePolygon(2,ii) belongs to
    % polygonNo - is the number of polygons that make up the domain
    
    EDGES_SORTED = e ; 
    
    % and we can assume that edges are in the 'right' order i.e. they are..
    % .. inserted as a consecutive columns in e matrix, polygon by polygon.

    %% handling the external boundary     
    % extract the edges that make up the external boundary 
    ii = 1 ;    
    EDGES_OF_EXT_POLYGON = EDGES_SORTED(:, (ePolygon(1,:) == index_of_external_polygon) ) ; 
    NODES_OF_EXT_POLYGON = EDGES_OF_EXT_POLYGON(1,:) ;
    P{index_of_external_polygon}.x = p(1, NODES_OF_EXT_POLYGON) ;
    P{index_of_external_polygon}.y = p(2, NODES_OF_EXT_POLYGON) ;
    
    % create Delaunay mesh based on the field points
    tri = delaunay(x,y) ;
    
    % compute coordinates of the midnodes of each triangle edge
    midNodes_x = [(x(tri(:,2))+x(tri(:,1)) )/2, ...
                  (x(tri(:,3))+x(tri(:,2)) )/2, ...
                  (x(tri(:,1))+x(tri(:,3)) )/2] ;
    midNodes_y = [(y(tri(:,2))+y(tri(:,1)) )/2, ...
                  (y(tri(:,3))+y(tri(:,2)) )/2, ...
                  (y(tri(:,1))+y(tri(:,3)) )/2] ;
    
    % auxiliary variable storing information about which triangles lie ...
    % outside the external boundary
    trianglesOutsideTheDomain = zeros(size(tri,1), 1) ; 
    
    % find which triangles (midpoints of triangles' edges) lie outside the
    % domain
    [INiithPolygon, ONiithPolygon] = ...
       inpolygon(midNodes_x,midNodes_y, P{index_of_external_polygon}.x,P{index_of_external_polygon}.y) ;
    % check if any midpoint of each triangle lies outside the domain and
    % not on the external boundary 
    iithPolygonMASK = ( any(xor(INiithPolygon,ONiithPolygon)') )' ;
    
    % update the information about the triangles outside the 1st polygon 
    % (i.e. outside the domain of interest)
    trianglesOutsideTheDomain = ~(trianglesOutsideTheDomain | iithPolygonMASK) ;
    
    
    %% handling the rest of the polygons (internal boundaries) provided they exist
    
    trianglesInsideHoles = zeros(size(tri,1), 1); % in case the are no holes
    
    if polygonNo > 1
        % If there are more contours than a one...
        % Polygons numbered from 2 on correspond to the holes inside the
        % domain (external polygon). All the triangles of the mesh that lie
        % inside those internal polygons should be identified and the 
        % information about them should be stored (trianglesInsideHoles 
        % variable which is a boolean mask) in order to delete them.
        
        % extract the ii-th polygon coordinates into separate cell elements
        for ii = 1:polygonNo
           if (ii ~= index_of_external_polygon )
           EDGES_OF_iith_POLYGON = EDGES_SORTED(:, (ePolygon(1,:) == ii) ) ; 
           NODES_OF_iith_POLYGON = EDGES_OF_iith_POLYGON(1,:) ;

           P{ii}.x = p(1, NODES_OF_iith_POLYGON) ;
           P{ii}.y = p(2, NODES_OF_iith_POLYGON) ;
           end
        end

        % check whether all contours lie inside the external one 
        FLAG = 1 ;
        ii = 1 ;
        while FLAG && ii<=polygonNo
           if (ii ~= index_of_external_polygon )
                FLAG = all( inpolygon(P{ii}.x, P{ii}.y, ...
                P{index_of_external_polygon}.x, P{index_of_external_polygon}.y) ) ;
           end
           ii = ii+1 ;
        end

        if ~FLAG
            error(['Check the geometry carefully. ' 
                   'The application does not accept disjoint domains.']) ;
        end

       % compute coordinates of the midnodes of each triangle edge
       midNodes_x = [(x(tri(:,2))+x(tri(:,1)) )/2, ...
                     (x(tri(:,3))+x(tri(:,2)) )/2, ...
                     (x(tri(:,1))+x(tri(:,3)) )/2] ;
       midNodes_y = [(y(tri(:,2))+y(tri(:,1)) )/2, ...
                     (y(tri(:,3))+y(tri(:,2)) )/2, ...
                     (y(tri(:,1))+y(tri(:,3)) )/2] ;

       % auxiliary variable storing information about which triangles ...
       % ... lie inside holes
       trianglesInsideHoles = zeros(size(tri,1), 1) ;   

       for ii=1:polygonNo
           
           if ii ~= index_of_external_polygon
               
           [INiithPolygon, ONiithPolygon] = ...
               inpolygon(midNodes_x,midNodes_y, P{ii}.x,P{ii}.y) ;
           % check if any midpoint of each triangle lies inside ...
           % ... ii-th hole _and_not_on_ i-th hole boundary
           iithPolygonMASK = ( any(xor(INiithPolygon,ONiithPolygon)') )' ;
           
           % store the information about the triangles inside internal holes
           trianglesInsideHoles = trianglesInsideHoles | iithPolygonMASK ;
           
           end
       end

    end

   % the triangles that lie outside the domain or inside the holes are
   % removed from the mesh.
   tri( trianglesOutsideTheDomain | trianglesInsideHoles,:) = [] ;
end

h = trisurf(tri, x, y, z, 'EdgeColor','none', 'FaceColor','interp');
        
end % function surfScatteredData(x,y,z)