function [nodes,edges_nodes] = CreateBEMesh(p,e)
% Function CREATEBEMESH: 
% - receives the list of all nodes and exterior edges (p,e)
% - removes the internal edges that may be present because of the
% definition of the domain as a union of simplex shapes in pdetool
% - reshapes the edges matrix to always have the domain on their left
% - detects the closed polygons that form the structural geometry
% - removes the nodes that are not exterior from the 'p' matrix
% - output delivered to the caller: NODES - list of the coordinates of the
% exterior nodes: EDGES_NODES - list of the initial and final nodes of each
% edge, with the orientation set such as to always have the domain on the
% left; 
% - output written in mat files: E - new edge structure, reordered and
% cleaned of interior edges; EPOLYGON - list with the edges' ID and the
% closed polygon to which they belong; POLYGONNO - the total number of
% closed polygons that define the geometry.

%% This bit was copied from dBMT_BC1

% ********************* START ***************************
% for non-regular meshes, reordering the edges to have the domain on their
% left and removing the interior edges caused by the union of domains in
% the mesh generator

%%
% check whether edges structure 'e' contains any interior edges
% (it may happen if domain is built as a union of more than one
% simplices)
% if the edges have simplices on both sides, remove columns corresponding to them
mask = ( (e(6,:) ~= 0) & (e(7,:) ~= 0) ) ;
e(:,mask) = [] ;

%%
% Reshaping e such as to always have the domain on their left
mask=(e(6,:)==0); % detects edges with the wrong orientation (having an element on the right)
e([1 2 6 7],mask)=e([2 1 7 6],mask); % switches to the right orientation

%%
% Analyse the geometry of the mesh and detects the polygons that define it
[e, ePolygon, polygonNo, index_of_external_polygon] = DetectPolygonsInPETMesh(e,p) ;


% % % % % % % % % load('dBMT_StructDef', 'UI') ;

save('irreg_mesh', 'e', 'ePolygon','polygonNo', 'index_of_external_polygon') ;

% ************************* FINISH *****************************

% Separating the first two columns of the 'e' matrix. They collect the
% nodes of the exterior edges
e = e(1:2,:);
enew = e;

% Sweeping the elements of the p matrix and checking if they exist in the e
% matrix. If they don't, that means they are not exterior nodes, so they
% are removed from the p matrix and the indices in the e matrix must be
% changed accordingly
todelete = zeros(1,size(p,2)); % 0 = not to delete; 1 = delete
for ii = 1:size(p,2)
    if ~any(any(e == ii))  % ii does not exist in the exterior edges' nodes
        todelete(ii) = 1; % schedule from deletion the column from p
        enew(e>ii) = enew(e>ii)-1; % subtract 1 from the edges indices larger than ii (read from the unchanged e matrix!!)
    end
end
% deleting the columns of p scheduled for deletion
p(:,logical(todelete))=[];

% delivering edges_nodes and nodes matrices
edges_nodes = enew';
nodes = p';

