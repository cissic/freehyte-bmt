% function [NGP, Nodes, Edges, Loops, BConds, MeshOption, offset, ...
%     SolMethod, lambda, EquationType, ProblemType, k, incident_params] ...
%                                                                 = InputProc
function [NGP, Nodes, Edges, Loops, BConds, UI] = InputProc


% INPUTPROC is the input processing function.
%
% INPUTPROC is called by MAIN. It reads the input data inserted in 
% the GUIs and organizes it in the data structures Edges, Loops and BConds.
%
%
% BIBLIOGRAPHY
% 1. FreeHyTE GIT Page 
% 2. Borkowski M, Moldovan ID - Direct Boundary Method Toolbox for some
% elliptic PDEs in FreeHyTE framework, submitted to Advances in Engineering
% Software, 2019
%
%
% INPUT DATA (read from the *.mat files created by the GUIs):
% * from dBMT_StructDef: k thermal conductivity coefficient  and Q internal 
% generated heat, NumberGaussPoints (the number of Gauss-Legendre points
% for the side integration), EdgesOrder (the order of the approximation
% bases on the boundary elements); 
% * from dBMT_BC1: nodes, edges_nodes. These variables list the coordinates
% of the boundary nodes and the nodes of the boundary elements,
% respectively; 
% * from HeatTriBC2: edgesDirichlet, edgesNeumann, dataDir, dataNeu;
% * edgesDirichlet (edgesNeumann): list with the Dirichlet (Neumann) edges;
% * dataDir (dataNeu): cell array with two columns and as many lines as
% Dirichlet (Neumann) edges. For each edge, it stores the edge's index, 
% and the Dirichlet (Neumann) boundary conditions. The boundary conditions
% are stored as strings. For further details regarding the definition of
% the boundary conditions, please refer to reference [2].
%
%
% OUTPUT DATA (to function MAIN):
% * NGP is the number of Gauss points for the line integration;
% * Nodes is a (NNODE x 2) matrix, where NNODE is the number of nodes in 
% mesh. It stores the coordinates of each node;
% * Edges, Loops and BConds are data structures storing information
% on the edges, domain and boundary conditions, respectively. 
% * offset is the offset of the interpolation nodes of the boundary
% elements from their geometric limits, expressed as a ratio of the length
% of the boundary elements
% * SolMethod designates which solution method should be used (1 - DTHM; 2-
% DMFS; 3 - DBEM)
% * lambda is the distance between real and auxiliary boundaries in the
% dMFS
% EquationType designates the type of problem that should be solved (1 -
% Laplace problem; 2- Helmholtz problem)
% ProblemType designates the type of problem that should be solved (1 -
% interior problem; 2- exterior problem)
% kparam is the wave number (k) parameter in Helmholtz problems. Not used
% in Laplace problems
% incident_params is a structure containing incoming plane wave parameters

%% Loading information GUI
load('dBMT_StructDef','UI');

% k = UI.k ;
% NumberGaussPoints = UI.NumberGaussPoints;
% EdgesOrder = UI.EdgesOrder ;
% offset = UI.offset ;
% MeshOption = UI.MeshOption ;
% SolMethod = UI.SolMethod ;
% lambda = UI.lambda ;
% EquationType = UI.EquationType ;
% ProblemType  = UI.ProblemType ;
% 
% incident_params.amplitude = UI.incident.amplitude ;
% incident_params.angle = UI.incident.angle ;


load('dBMT_BC1','nodes','edges_nodes');
load('dBMT_BC2','edgesDirichlet','edgesNeumann','edgesRobin',...
    'dataDir','dataNeu','dataRobin');




%% MESH DATA
% Creation of the Nodes, Edges and Loops data structures

Nodes = nodes;

% Definition of structures Edges and Loops. 
Edges=struct('nini',edges_nodes(:,1),'nfin',edges_nodes(:,2),...
    'parametric',createLine(Nodes(edges_nodes(:,1),:),...
    Nodes(edges_nodes(:,2),:)),...
    'type',char(zeros(length(edges_nodes(:,1)),1)),'order',...
    zeros(length(edges_nodes(:,1)),1));
Edges.type(:) = 'D';        %all edges are predefined as Dirichlet
Edges.order(:) = NaN;       %all degrees are predefined as NaN

% In the BEM context, the Loops structure refers to the whole domain.
Loops = struct('center',zeros(1,2),'area',0);

% Computation of the barycenter and area of the domain:     
% It uses the POLYGONCENTROID function by David Legland.
[Loops.center,Loops.area] = polygonCentroid(Nodes);
Loops.area=abs(Loops.area);

%% RUN CONTROL DATA
%Number of Gauss integration points per boundary
NGP = UI.NumberGaussPoints;   

%% EDGE TYPE DATA
% Registration of the Neumann and Robin edges, using edgesNeumann and
% edgesRobin vector from the GUI.
% It is recalled that all edges were predefined as Dirichlet.
% Users may overwrite the data loaded from the GUI to change the boundary
% types. 
if exist('edgesNeumann')
    for ii=1:length(edgesNeumann)
        Edges.type(edgesNeumann(ii),1) = 'N';
    end
end
if exist('edgesRobin')
    for ii=1:length(edgesRobin)
        Edges.type(edgesRobin(ii),1) = 'R';
    end
end

%% EDGE REFINEMENT DATA
% Allocate the refinement order defined in the GUI to all Dirichlet
% boundaries ...
Edges.order(:) = UI.EdgesOrder;

 
%% State and flux data 
% Boundary conditions can be described by polynomials of any order. The 
% definition of a boundary condition is made by specifying its values in 
% as many equally spaced points along the boundary as needed to define its
% polynomial variation. For further details regarding the definition of the 
% boundary conditions, please refer to reference [2].
%
% BConds data structure collects information regarding the boundary 
% conditions. Its members are cell arrays with as many lines as the 
% external boundaries of the structure. The values of the fluxes (or
% potentials) enforced on the boundaries are stored in the Neumann (or
% Dirichlet) fields of the structure. NaN is stored in the Dirichlet field
% of a Neumann boundary, and vice-versa.

% Initialization of the BConds structure
BConds = struct('Neumann',{cell(length(edges_nodes),1)},'Dirichlet',...
    {cell(length(edges_nodes),1)},'Robin', {cell(length(edges_nodes),2)});
BConds.Neumann(:) = {NaN};
BConds.Dirichlet(:) = {NaN};
BConds.Robin(:,:) = {NaN};

% Dirichlet boundary conditions are imported from the GUI and stored in the
% Dirichlet field of the structure. Users may overwrite the data loaded
% from the GUI to change the boundary conditions. 
if exist('dataDir')
    for i=1:size(dataDir,1)
        BConds.Dirichlet{dataDir{i,1},1}=str2num(dataDir{i,2});
    end
end

% Neumann boundary conditions are imported from the GUI and stored in the
% Neumann field of the BConds structure. Users may overwrite the data
% loaded from the GUI to change the boundary conditions. 
if exist('dataNeu')
    for i=1:size(dataNeu,1)
        BConds.Neumann{dataNeu{i,1},1}=str2num(dataNeu{i,2});
    end
end

% Robin boundary conditions are imported from the GUI and stored in the
% Robin field of the BConds structure. Users may overwrite the data
% loaded from the GUI to change the boundary conditions. 
if exist('dataRobin')
    for i=1:size(dataRobin,1)
        BConds.Robin{dataRobin{i,1},1}=str2num(dataRobin{i,2});
        BConds.Robin{dataRobin{i,1},2}=str2num(dataRobin{i,3});
    end
end

% NoDiv is the number of points for plotting the colormaps of the
% solution, in each Cartesian direction. It is predefined here as the
% number of Gauss points used for the side integration, but it may be
% arbitrarily defined to some other value.
NoDiv = NGP;

end
