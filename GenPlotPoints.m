function PointsCoord = GenPlotPoints(abscissa)
% Loads the mesh information from the GUIs and delivers the global
% coordinates of the points where the solution must be calculated. 
% There are length(abscissa)^2 plot points per subdomain, meaning that the
% total number of plotting points is NSD*length(abscissa)^2.
% For regular domains, the subdomains are rectangular and the plotting
% points correspond to the locations defined by ABSCISSA.
% For non-regular domains, the subdomains are triangular and the plotting
% points are GAUSS QUADRATURE points. For their calculation, the values in
% ABSCISSA are irrelevant, only its length being sufficient for Gauss
% points' generation.

%%
% Loading the mesh information.
% MeshOption == 1(2) means regular (non-regular) mesh
load('dBMT_StructDef','UI','p','t');

MeshOption = UI.MeshOption ;
L = UI.L ;
B = UI.B ;
Nx = UI.Nx ;
Ny = UI.Ny ;

% initialize PointsCoord
PointsCoord = [];

if MeshOption == 1 % Regular mesh
    % Generate finite element information. CreateRegMesh is launched in the
    % 'post' mode to avoid the removal of the nodes from inside the domain.
    [nodes, ~, ~, loops_nodes] = CreateRegMesh(L,B,Nx,Ny,'post');
    
    % start sweeping the loops
    for ii=1:length(loops_nodes(:,1))
        
        [centroid,~] = polygonCentroid(nodes(loops_nodes(ii,:),:));
        
        % Computing the length of the sides of the element in x and y direction.
        % sze simply collects the distances between the two most far apart points
        % of the element in x and y directions. ASSUMES THAT THE ELEMENT IS
        % RECTANGULAR!!
        sze = max(nodes(loops_nodes(ii,:),:)) - min(nodes(loops_nodes(ii,:),:));
        
        % Generating the local x and y coordinates of Gauss collocation
        % points. Local coordinates are computed in respect to the centroid
        % of the current element
        loc_x=1/2*sze(1)*abscissa;
        loc_y=1/2*sze(2)*abscissa;       
        [x,y] = ndgrid(loc_x,loc_y);
        
        % Global coordinates
        globalx = x + centroid(1);
        globaly = y + centroid(2);
        
        % update PointsCoord
        PointsCoord = cat(1, PointsCoord,[globalx(:) globaly(:)]);
        
    end
    
else   % Irregular mesh - the calculation nodes are Gauss points
    % Generate finite element mesh information
    [nodes, ~, ~ , loops_nodes, ~] = CreateEdgeLoop(p,t);
    NGP = length(abscissa);
    
    % start sweeping the loops
    for ii=1:length(loops_nodes(:,1))
                
        % Getting coordinates of the nodes of the element (global)
        LocNodes = nodes(loops_nodes(ii,:),:);
        % Generating the Gauss points (global)
        [globalx,globaly,~,~]=triquad(NGP,LocNodes);
        
        % update PointsCoord
        PointsCoord = cat(1, PointsCoord,[globalx(:) globaly(:)]);
        
    end
end


end
