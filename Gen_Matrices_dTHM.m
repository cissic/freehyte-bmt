function [G, H, R] = Gen_Matrices_dTHM(Edges, BConds, abscissa, weight, ...
            offset, preProcStruc, Helm_kappa, Dim, probType)
     
% GEN_MATRICES_DTHM sweeps through the elements and calls the function
% that generate the H and G block matrices for direct Trefftz method
% (Trefftz-Herrera method)
%
% Gen_GandH_Matrices_dTHM is called by MAIN***. 
% Input data: 
% the Edges, BConds -- structures that describe the problem
% abscissa, weights    - the Gauss-Legendre integration parameters
% offset               - the offset of the interpolation nodes of the boundary
%                          elements from their geometric limits
% preProcStruc         - auxiliary structure that stores collocation nodes,
%                         domain characteristic length, and optimal place for 
%                         Trefftz eigenexpension (domain barycenter)
% Helm_kappa           - if Helm_kappa == 0 => Laplace solver is invoked
%                        if Helm_kappa ~= 0 => Helmholtz solver is invoked
% Dim                  - dimension of the system of equations
% probType             - 'int' or 'ext' defines interior or exterior type
%                        of problem


% Output/returns to MAIN***:
% G,H (and R if necessary) - matrices for dTHM ( Eqs.(15-17) in ref. [3])
%
% BIBLIOGRAPHY
% 1. C.-S. Liu. An effectively modified direct Trefftz method for 
%     2D potential problems considering the domain's characteristic length. 
%     Engineering Analysis with Boundary Elements, 31(12):983-993, 2007
% 2. Borkowski M, Moldovan ID - On rank-deficiency in direct Trefftz method 
%    for 2D Laplace problems, Engineering Analysis with Boundary Elements,
%    106, 102–115, (2019)
% 3. Borkowski M, Moldovan ID - Direct Boundary Method Toolbox for some
%    elliptic PDEs in FreeHyTE framework, submitted to Advances in 
%    Engineering Software, 2019

if ~(strcmp(probType, 'int') || strcmp(probType,'ext'))
    error("Unspecified probType. Only 'int' and 'ext' values are possible.")
end

% dTHM specific computations: 
R0 = preProcStruc.dChL/2 ;              % scaling factor see ref. [1]
TreffC = preProcStruc.TreffC ;          % center of Trefftz eigenexpansion
% no of interpolation points = no of columns of H and G matrices
[P,~] = size(preProcStruc.collNodes) ;

% Initialization of the matrices - calculating order of the Trefftz basis 

N = (Dim(1)-1)/2 ;

% Currently function _always_ generates odd number of rows, so there is a
% need to use pinv solver (when number of interpolation nodes is even). 
% This can be modified in future versions.

% Rows = 2*N + 1 ; % no of rows in matrices = no of weighting functions 
temporaryRows = N + 1 ;

% Initialization of the matrices

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Determining some details concerning Robin BC

% There is a need for calculating R vector only if Robin BC 
% du_dn = g(S) + ru
% is imposed at at least one boundary element _and_ function g(S) is
% non-zero there.

RVectorFLAG = 0 ; % flag that informs whether R vector is needed

if any(Edges.type == 'R') % if there is any element with Robin BC prescribed
    try
       gFunValues = cell2mat(BConds.Robin(:,1)) ;
       
       if ~all( ((gFunValues == 0)+isnan(gFunValues))==1 )
           % if there's at least one value of g function
           % that is (nonzero and (not NaN) )
           RVectorFLAG = 1 ;
       end

    catch ME 
       if (strcmp(ME.identifier,'MATLAB:catenate:dimensionMismatch'))
          % if the line 
          % gFunValues = cell2mat(BConds.Robin(:,1)) ;
          % throws an exception, probably the g function prescribed on 
          % one of the boundary elements is of higher degree
          % than zero -> vector R needs to be created
          RVectorFLAG = 1 ;      
       end
       %rethrow(ME)
    end 
end
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% preallocate space for final matrices
H = zeros(temporaryRows,P) ;
G = zeros(temporaryRows,P) ;
if RVectorFLAG
   R = zeros(temporaryRows,1) ; 
else
   R = [] ;
end


%% Sweeping the edges
for ii=1:length(Edges.nini)
        % LocLoop is a structure where the features of the current
        % element which are directly useful for the calculation of the
        % local blocks of H and G matrices are stored.
        LocEdge =  struct('id',ii,'parametric',Edges.parametric(ii,:),...
            'order',Edges.order(ii),'insert',Edges.insert(ii),...
            'dim',Edges.dim(ii));     
        
        % Computing the Gi and Hi matrices of element ii. GHR_Matrix_i is
        % a local function (see below).
        if RVectorFLAG && (~any(isnan(BConds.Robin{ii,1})) && ~all(BConds.Robin{ii,1}==0) ) 
            % if this the element where g function is non-zero
            [Gi, Hi, Ri] = GHR_Matrix_i(LocEdge, abscissa, weight, offset, ...
                Dim, R0, TreffC, Helm_kappa, probType, BConds) ;
            % update R vector
            R = R + Ri ;
        else
            % if this is the element where g function does not exist
            [Gi, Hi] = GHR_Matrix_i(LocEdge, abscissa, weight, offset, ...
                Dim, R0, TreffC, Helm_kappa, probType) ;    
        end
        
        % Inserting the matrix in the global matrices. The insertion is made
        % at line & column LocEdge.insert(ii).
        G(:,LocEdge.insert:LocEdge.insert+LocEdge.dim-1) = Gi;
        H(:,LocEdge.insert:LocEdge.insert+LocEdge.dim-1) = Hi;
end

% split real and imaginary parts of the matrices into different rows to
% generate a final form of matrices
G = [real(G); imag(G(2:end, :)) ] ;
H = [real(H); imag(H(2:end, :)) ] ;  % <- moved from GH_Matrix_i function
if ~isempty(R)
    R = [real(R); imag(R(2:end, :)) ] ; 
end

end


function [Gi, Hi, vargout] = GHR_Matrix_i(LocEdge, abscissa, weight, offset, ...
            Dim, R0, TreffC, Helm_kappa, probType, BConds)
% GH_Matrix_i is a local function that computes the G and H blocks of the LocEdge element. 
% The boundary elements are mapped to a [-1,1] interval to perform the integrations.
% The integrations are conducted with the use of the following 3D arrays:

%              . . . . .
%             . . . . .
%            /_/_/_/|.
%           /_/_/_/|/   quadrature nodes no (Q)
%          /_/_/_/|/
%         /_/_/_/|/
% Trefftz |_|_|_|/  .
%  weight |_|_|_|/ . 
% funs no |_|_|_|/.
%   (R)   |_|_|_|/
%         |_|_|_|/
%          .
%          .
%          .
%          shape
%          funs
%           no (M)

% R0
% TreffC

%% Initialization 
    % choose appropriate weighting function (and their respective 
    % derivatives in r and th) depending on the type of
    % problem -> Eqs.(26-27) in ref. [3]
    
    if strcmp(probType, 'int')
        if  Helm_kappa == 0  % interior 2D Laplace problem
            % Applying Liu's scaling [1], and ref [3] -> Eq.(28)
            LiuScaling = @(n) (1/R0).^abs(n) ;

            TH_fun 	   =  @(th,r,n)          r.^abs(n)     .* exp(1i*abs(n).*th) .* LiuScaling(n) ;
            TH_fun_dr   = @(th,r,n) abs(n).* r.^(abs(n)-1) .* exp(1i*abs(n).*th) .* LiuScaling(n) ;
            TH_fun_dt   = @(th,r,n)1i * n .* r.^(abs(n)-1) .* exp(1i*abs(n).*th) .* LiuScaling(n) ;

        else                % interior 2D Helmholtz problem       
            kap = Helm_kappa ; % sqrt(Helm_kappa) ;

            TH_fun      = @(th,r,n)                                                besselj(abs(n), kap*r) .* exp(1i*th.*n) ;
            TH_fun_dr   = @(th,r,n)        kap/2 * (besselj(abs(n)-1, kap*r) - besselj(abs(n)+1, kap*r) ) .* exp(1i*th.*n) ;
            TH_fun_dt   = @(th,r,n) 1i * n .*  (r).^(-1) .*                        besselj(abs(n), kap*r) .* exp(1i*th.*n) ;
        end
    else % strcmp(probType, 'ext')
        if  Helm_kappa == 0  % exterior 2D Laplace problem
            % ln(r) as a first function is called explicitly in the lines
            % below
            TH_fun 	   =  @(th,r,n)          r.^-abs(n)     .* exp(1i*abs(n).*th)  ;
            TH_fun_dr   = @(th,r,n) abs(n).* r.^(abs(n)-1) .* exp(1i*abs(n).*th)  ;
            TH_fun_dt   = @(th,r,n)1i * n .* r.^(abs(n)-1) .* exp(1i*abs(n).*th)  ;

        else                % exterior 2D Helmholtz problem       
            kap = Helm_kappa ; % sqrt(Helm_kappa) ;

            TH_fun      = @(th,r,n)                                                besselh(abs(n), kap*r) .* exp(1i*th.*n) ;
            TH_fun_dr   = @(th,r,n)        kap/2 * (besselh(abs(n)-1, kap*r) - besselh(abs(n)+1, kap*r) ) .* exp(1i*th.*n) ;
            TH_fun_dt   = @(th,r,n) 1i * n .*  (r).^(-1) .*                        besselh(abs(n), kap*r) .* exp(1i*th.*n) ;
        end 
    end
        
    % no of interpolation nodes/shape functions on element
    M = LocEdge.order +1 ;  
        % total no of interpolation nodes
        % P = M*length(LocEdge) ;
         % order of the Trefftz basis 
        N = (Dim(1)-1)/2 ; 
    % no of rows in matrices
    NVec = (0:N)' ; %(-N:N)' ;
    Rows = length(NVec) ;    
    % no of Gauss interpolation nodes
    Q = length(abscissa) ;
    
    % final size of structures used in integration is [Rows x M x Q]     
    

    %% Interpolation nodes and shape functions
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    % Generating local coordinates of interpolation nodes and respective 
    % shape functions on element: 
    % interpolation_nodes_distribution = 0 if (LocEdge.order == 0) % constant element
    % interpolation_nodes_distribution is a vector of equidistant points in 
             % the interval [-1+offset,1-offset] for  higher order elements
    % m = 0:LocEdge.order;

    interpolation_nodes_distribution = (LocEdge.order~=0)*...
        linspace(-1+offset,1-offset, LocEdge.order+1) ;
    
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    %%%% Computing shape functions
    [S] = shapeFun1D(abscissa, interpolation_nodes_distribution ) ; % NOTE: line can be moved outside this function and passed as parameter
    % reshaping shape functions to match proper size of 3D grid constructed
    % for integration 
    S3DVec = reshape(S, 1, M, Q ) ;
    
    %% Generating the geometric data

    % Computing the length of the current edge
    L = sqrt((LocEdge.parametric(3))^2 + (LocEdge.parametric(4))^2);   % NOTE: this can be calculated in preProcessing function and passed here as parameter 
    % reshaping abscissa vector to match proper size of 3D itegration grid
    A3DVec = reshape(abscissa, 1,1, Q) ; 

    % Getting global coordinates and components of normal vectors for all Gauss points
    X3DVec = (LocEdge.parametric(1) - TreffC(1) + 0.5 * (A3DVec + 1) * LocEdge.parametric(3) ) ;  
    Y3DVec = (LocEdge.parametric(2) - TreffC(2) + 0.5 * (A3DVec + 1) * LocEdge.parametric(4) ) ;
    
    % Transforming the local Cartesian coordinates into polar 
    R3DVec = hypot(X3DVec,Y3DVec) ;
    Th3DVec= atan2(Y3DVec,X3DVec) ;
    %%% ^ X3DVec,Y3DVec,R3DVec, Th3DVec are [R x 1 x Q] arrays 
    
    %% Computing the values of weighting functions for all integration points 

    % predefining auxiliary structures for array-like computation of weighting functions
    [R3D, NVec3D] = meshgrid(R3DVec, NVec) ;
    R3D = reshape(R3D, Rows,1, Q) ;
    T3D = repmat(Th3DVec,Rows, 1) ;
    N3D = reshape(NVec3D, Rows,1, Q) ;

    % weighting functions for G matrix
    W = TH_fun(T3D, R3D, abs(N3D) ) ;
    
    % weighting functions for H matrix
        % Computing the components of the outward normal in the Cartesian directions (H matrix).
        nx = LocEdge.parametric(4) / L;
        ny = -1* LocEdge.parametric(3) / L;

        % Computing the components of the outward normal in the polar directions (H matrix).
        NR3DVec  = nx * cos(T3D) + ny * sin(T3D) ;
        NTh3DVec = -1*nx * sin(T3D) + ny * cos(T3D);
          
        % Polar components of the flux basis: 
        DwDr  = TH_fun_dr(T3D, R3D, abs(N3D) ) ;
        DwDth = TH_fun_dt(T3D, R3D, abs(N3D) ) ;

        % Normal derivative
    DwDn = bsxfun(@times, NR3DVec, DwDr) + bsxfun(@times, NTh3DVec, DwDth) ;
    
    if strcmp(probType, 'ext') && Helm_kappa == 0 
       % if exterior Laplace problem we need to call ln(r) explicitly as
       % the first weighting function (in fact, values in row 1 of arrays 
       %computed in lines 269 and 286 were miscalculated!)
       W(1,:,:) = log(R3D(1,:,:)) ;
       DwDr = 1 ./ (R3D(1,:,:)) ;
       DwDn(1,:,:) = DwDr .* NR3DVec(1,:,:)  ;
    end
    
%% Performing the side integration and updating the field values
    % Preparing weights for integration 
    w3DVec(1,1,:) = weight;

    % constructing integrands
    IntegrandG = bsxfun(@times, W   ,S3DVec) ; 
    IntegrandH = bsxfun(@times, DwDn,S3DVec) ; 
    
    Gi = L/2 * sum(bsxfun(@times,IntegrandG,w3DVec),3);
    Hi = L/2 * sum(bsxfun(@times,IntegrandH,w3DVec),3);
    

    if nargout > 2
        % then this is an element with Robin BC ->
        % there is a need to calculate local component of R vector
        
        % obtaining the equally spaced points on [-1,1] interval where 
        % g function values are defined and stored in BConds.Robin
        a = linspace(-1,1,length( BConds.Robin{LocEdge.id,1} ));

        % obtaining the polynomial that interpolates the values in BConds.Robin
        if (isnan( BConds.Robin{LocEdge.id,1} ))
            error('local:consistencyChk',...
                'No Robin boundary conditions are defined on edge %d. \n',...
                LocEdge.id);
        else
            pol = polyfit(a, BConds.Robin{LocEdge.id,1},...
                length( BConds.Robin{LocEdge.id,1} )-1);
        end

        % computing the values of the interpolation polynomials at
        % quadrature nodes
        g = polyval(pol,abscissa);
        g = reshape(g,1,1,Q) ; 

        IntegrandR = bsxfun(@times,g,W) ; 
        Ri = L/2 * sum(bsxfun(@times,IntegrandR,w3DVec), 3) ;
        
        vargout = Ri ;
    end

end
