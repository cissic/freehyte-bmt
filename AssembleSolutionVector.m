function [uGlob, qGlob] = AssembleSolutionVector(x, known, Edges, BConds, offset) 

% ASSEMBLESOLUTIONVECTOR reconstructs vectors uGlob, qGlob basing on 
% given boundary conditions and results obtained from solving system
% of equations

% AssembleSolutionVector is called by MAIN***. 
% Input data: 
% x             - solution of system of equations
% Edges         - structure that stores information about boundary elements

% BConds        - structure that stores information about boundary elements
%                     it is needed only if Robin BC exist
% offset        - the offset of the interpolation nodes of the boundary
%                          elements from their geometric limits, 
%                     it is needed only if Robin BC exist

% Output/returns to MAIN***:
% uGlob, qGlob - values of double and single layer potentials at
%                 interpolation nodes 
%
% BIBLIOGRAPHY
% 1. Borkowski M, Moldovan ID - Direct Boundary Method Toolbox for some
%    elliptic PDEs in FreeHyTE framework, submitted to Advances in 
%    Engineering Software, 2019

% prepare vectors for single and double layer potential node values
uGlob = NaN*ones(size(x)) ;
qGlob = NaN*ones(size(x)) ;

% since we assume the same interpolation degree on all BEs which is ...
noOfIntNodesOnEachBE = Edges.order(1)+1 ;
% ... we can generate auxiliary vector that stores information about BC type
% prescribed at all interpolation nodes by simply repmatting BC types
% prescribed on each boundary element by repmatting Edges.type vector and 
% vectorizing the result
temp = ( repmat(Edges.type,1,noOfIntNodesOnEachBE) )' ;
BCTypeInIntNodes = temp(:) ;

% for nodes with Dirichlet and Neumann BC obtaining the final vector with
% nodal values of single and double layer potential is easy and it comes
% down to reassigning corresponding values of known and x vectors to uGlob
% and qGlob vectors
uGlob(BCTypeInIntNodes=='D') = known(BCTypeInIntNodes=='D') ;
uGlob(BCTypeInIntNodes=='N') = x(BCTypeInIntNodes=='N') ;
qGlob(BCTypeInIntNodes=='N') = known(BCTypeInIntNodes=='N') ;
qGlob(BCTypeInIntNodes=='D') = x(BCTypeInIntNodes=='D') ;


% if there exist Robin BC additional computations need to be conducted
if nargin > 3 
   
   % find the elements with Robin BC prescribed on them
   indOfBEwithRobinBC = find(Edges.type == 'R') ;
   
   for ii = indOfBEwithRobinBC'   % for each element with Robin BC
       LocEdge =  struct('id',ii,'type',Edges.type(ii),...
        'order',Edges.order(ii),'insert',Edges.insert(ii),...
        'dim',Edges.dim(ii));
       
        % loading r parameter of Robin BC: q = g(s) + ru -> Eq.(4) in ref.[1]
        if ( isnan( BConds.Robin{LocEdge.id,2}) ) 
            error('local:consistencyChk',...
                'No Robin boundary conditions are defined on edge %d. \n',...
                LocEdge.id);
        else
            r = BConds.Robin{LocEdge.id,2} ;           
            
            % u for node with Robin BC prescribed is a value stored in 
            % corresponding element of x vector 
            uGlob(LocEdge.insert:LocEdge.insert+LocEdge.dim-1,1) =         x(LocEdge.insert:LocEdge.insert+LocEdge.dim-1,1) ; 
            % q = g(s) + ru -> 
            %                   calculate ru term:
            qGlob(LocEdge.insert:LocEdge.insert+LocEdge.dim-1,1) = r * uGlob(LocEdge.insert:LocEdge.insert+LocEdge.dim-1,1) ;

            % q = g(s) + ru -> 
            %                   calculate g(s) contribution if g(s) ~= 0 
            if (~any(isnan(BConds.Robin{ii,1})) && ~all(BConds.Robin{ii,1}==0) )      

                % getting the points for the calculation of the boundary fields
                interpolation_nodes_distribution = ...
                    (LocEdge.order~=0)*linspace(-1+offset,1-offset, LocEdge.order+1) ;

                % obtaining the equally spaced points on [-1,1] interval where the
                % temperatures are defined and stored in BConds.Neumann
                a = linspace(-1,1,length(BConds.Robin{LocEdge.id,1}));
                pol = polyfit(a,BConds.Robin{LocEdge.id,1},...
                    length(BConds.Robin{LocEdge.id,1})-1) ;

                % computing the values of the interpolation polynomials at the 
                % interpolation_nodes_distribution
                g = polyval(pol,interpolation_nodes_distribution);
                g = g.'; % non-conjugate transpose   

                % update the values of q with g(s) contribution
                qGlob(LocEdge.insert:LocEdge.insert+LocEdge.dim-1,1) = qGlob(LocEdge.insert:LocEdge.insert+LocEdge.dim-1,1) + g ;
            end
        end
   end   
end



