function [nodes, edges_nodes, edges_loops, loops_nodes] ...
    = CreateRegMesh(L,B,Nx,Ny,~)
% CREATEREGMESH generates subdomain mesh information for regular, rectangular
% meshes.
% * INPUT: domain's dimensions (L,B) and number of finite elements in
% the Cartesian directions (Nx,Ny). A fifth argument is input if the
% function is called in the post-processing stage, to avoid the elimination
% of the nodes that are not on the boundary.
% * OUTPUT: 
%   + nodes: list with the nodes' Cartesian coordinates;
%   + edges_nodes: list with the indices of the nodes at the beginning
%   and end of an edge;
%   + edges_loops: list with the subdomains on the left and right of
%   each edge;
%   + loops_nodes: list with the indices of the nodes belonging to a
%   subdomain.

%% Generates the mesh information
nel = Nx*Ny ;        % Total Number of Elements in the Mesh
nnel = 4 ;           % Number of nodes per Element
% Number of points on the Length and Width
npx = Nx+1 ;
npy = Ny+1 ;
nnode = npx*npy ;      % Total Number of Nodes in the Mesh
% Number of edges on the Length and Width
nedgex = Nx*npy;       % Number of Horizontal Edges
nedgey = Ny*npx;       % Number of Vertical Edges
nedge = nedgex+nedgey; % Total Number of Edges in the Mesh

%% Obtaining the nodes associated to each subdomain (loops_nodes)
% Discretizing the Length and Width of the domain
nx = linspace(0,L,npx) ;
ny = linspace(0,B,npy) ;
[xx,yy] = meshgrid(nx,ny) ;
% To get the Nodal Connectivity Matrix
nodes = [xx(:) yy(:)] ;
NodeMap = 1:nnode ;
loops_nodes = zeros(nel,nnel) ;
% If subdomains along the X-axes and Y-axes are equal
if npx==npy
    NodeMap = reshape(NodeMap,npx,npy);
    loops_nodes(:,1) = reshape(NodeMap(1:npx-1,1:npy-1),nel,1);
    loops_nodes(:,2) = reshape(NodeMap(2:npx,1:npy-1),nel,1);
    loops_nodes(:,3) = reshape(NodeMap(2:npx,2:npy),nel,1);
    loops_nodes(:,4) = reshape(NodeMap(1:npx-1,2:npy),nel,1);
% If the subdomains along the axes are different
else
    NodeMap = reshape(NodeMap,npy,npx);
    loops_nodes(:,1) = reshape(NodeMap(1:npy-1,1:npx-1),nel,1);
    loops_nodes(:,2) = reshape(NodeMap(2:npy,1:npx-1),nel,1);
    loops_nodes(:,3) = reshape(NodeMap(2:npy,2:npx),nel,1);
    loops_nodes(:,4) = reshape(NodeMap(1:npy-1,2:npx),nel,1);
end

%% Obtaining the edge information (edges_nodes and edges_loops)
% Finding the nodes for each edge
edgesx=[reshape(NodeMap(1:npy-1,:),nedgey,1),...
    reshape(NodeMap(2:npy,:),nedgey,1)];
edgesx(1:Ny,:)=fliplr(edgesx(1:Ny,:));
edgesy=[reshape(NodeMap(:,2:npx),nedgex,1),...
    reshape(NodeMap(:,1:npx-1),nedgex,1)];
edgesy(1:npy:end,:)=fliplr(edgesy(1:npy:end,:));
edges_nodes = [edgesx ; edgesy];
%Finding the neighbouring subdomains for each edge
edges_loops = zeros(nedge,2);
for i=1:nedge
    [row1, ~] = find(loops_nodes==edges_nodes(i,1));
    [row2, ~] = find(loops_nodes==edges_nodes(i,2));
    edges_loops(i,:)=intersect(row1,row2)';
    if edges_loops(i,1)==edges_loops(i,2) % a single neighbour
        edges_loops(i,2)=0;              % right neighbour is set to zero
    elseif edges_loops(i,1) > edges_loops(i,2) % left neighbour larger than right
        edges_loops(i,:)=fliplr(edges_loops(i,:)); % flip them (left always
    end                                 % smallest in regular meshes)
end
%Finding the edges of each subdomain
loops_edges = zeros(nel,4);
for i=1:nel
    [row1, ~] = find(edges_loops==i);
    loops_edges(i,:)=row1';
end    

if nargin == 4 % if the function is called with 4 arguments, it's pre-processing.
    % Otherwise, it's post-processing and this part is not
    % performed
    %% Getting the exterior nodes
    % the boundary nodes are those nodes that belong to boundaries whose right
    % subdomain is zero (no right subdomain means they are exterior)
    nodes_on_boundary = edges_nodes(edges_loops(:,2) == 0,1);
    
    % coordinates of the boundary nodes
    x = nodes(nodes_on_boundary,1);
    y = nodes(nodes_on_boundary,2) ;
    
    %% Reordering the exterior nodes in the CCW direction
    % getting some interior 'middle point' to serve as a centre for reordering
    xmean = mean(x);
    ymean = mean(y);
    % getting the 'azimuth' of the boundary points in respect to the middle
    % point
    angles = atan2( (y-ymean) , (x-xmean));
    % sorting the exterior nodes according to the azimuth
    [~, sortIndices] = sort(angles);
    nodes_in_CCW_Direction = nodes_on_boundary(sortIndices,:) ;
    % reordering the exterior nodes
    nodes = nodes(nodes_in_CCW_Direction,:);
    
    % building the exterior edges information. It is assumed that exterior
    % edges connect consecutive exterior nodes, which is obviously not true for
    % many non-simply connected domains
    [noOfExtNodes,~] = size(nodes) ;
    edges_nodes = [1:noOfExtNodes ; [2:noOfExtNodes 1] ]' ;
end