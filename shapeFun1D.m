function [y, dy] = shapeFun1D(x, nORx, lambda)

% SHAPEFUN1D returns the values of 1D shape functions defined on normalized interval [-1,1]

% Input data: 
% x      - vector (or a matrix)
% nORx   - degree of interpolation polynomial/-s (if scalar) OR roots of shape polynomials (if vector)
% lambda - if given for scalar nORx it corresponds to the distance between
            % geometry nodes and outermost interpolation nodes 
            % does not have any effect for nORx being a vector

% Output data:
% y,dy   - each i-th row of matrix y (or dy) corresponds to the values of i-th
            % interpolation polynomial (or its derivative) (if x is a vector)

% y(i,:,k), dy(i,:,k) - each i-th row corresponds to the values of i-th interpolation
            % polynomial (or its derivative) for k-th different abscissa (if x is a matrix)
            



% define lambda if it is not given as a parameter
if (length(nORx) == 1) && nargin < 3 
   lambda = 0 ; 
end

if length(nORx) == 1
    % then x0 is a number of "equidistant" interpolation nodes on interval
    n = nORx ;
    xi = (n~=0) * linspace(-1+lambda,1-lambda, n+1) ;        
elseif length(nORx) > 1
    xi = nORx ;
    n = length(xi)-1 ;        
else 
    error('length of nORx cannot be negative.')
end

% Construct Vandermonde matrix.
A = bsxfun(@power,xi',n:-1:0) ;
% Construct RHS-s matrix
B = eye(n+1) ;

% each column of the solution P stores coefficients of interpolation polynomial
P = A\B ;   % inv(A) and pinv(A) are a bit slower here


if isvector(x)    
    % different n+1 shape functions for the same abscissa 
    x = x(:)' ;
    %x = repmat(x, n+1, 1) ;
    y = zeros( n+1, length(x) ) ;
    if nargout > 1
        dy = zeros( n+1, length(x) ) ;
    end
    
    
    for ii=1:n+1
        y(ii,:) = polyval(P(:,ii), x ) ;
        if nargout > 1
            dy(ii,:)= polyval(mypolyder(P(:,ii)), x) ;
        end
    end
    
else
    % each shape functions is calculated for different abscissa
    % x is matrix of (n+1) rows (or columns) of different abscissas with
    % the same length
   [rows,cols] = size(x) ; 
   if cols == n+1
       % transpose x to have n+1 rows of different abscissas
       x = x' ;
       abscissaLen = rows ;
   else  % x has already proper form
       abscissaLen = cols ;
   end
   x = reshape(x',1, abscissaLen, n+1) ;   % transpose is needed here
   y = zeros(n+1, abscissaLen, n+1) ;
   if nargout > 1
        dy = zeros(n+1, abscissaLen, n+1) ;
    end
   
   for ii=1:n+1
        y(ii,:,:) = polyval(P(:,ii), x ) ;
        if nargout > 1
            dy(ii,:)= polyval(mypolyder(P(:,ii)), x) ;
        end
   end
   
   
end

end % function [y, dy] = shapeFun1D(x, nORx, lambda)


%% Auxiliary function:
% Much faster computation of polynomial derivative than with Matlab's polyder()
function deriv = mypolyder(p)
    
    % reshape p vector to be a row vector
    p = p(:)' ;
    % polynomial order+1
    n = length(p) ;
    
    % current coeffficients (without the last, unimportant one) =  p(1:end-1) 
    if n>1
       deriv = p(1:end-1) .*  ( (n-1):-1:1 ) ;
    elseif n==1
       deriv = 0 ; 
    else
       error('This should not have happened.')
    end
end % function deriv = mypolyder(p)





