function [G, H, R] = Gen_Matrices_dMFS(Edges, BConds, abscissa, weight, ...
    offset, preProcStruc, Helm_kappa, lambda )

% GEN_MATRICES_DMFS sweeps through the elements and calls the function
% that generate the H and G block matrices for direct Method of
% Fundamental Solutions (aka regular direct Boundary Element Method, direct Trefftz Kupradze Method, ...) 

% Gen_GandH_Matrices_dMFS is called by MAIN***. 
% Input data: 
% the Edges and BConds -- structures that describe the problem
% abscissa, weights    - the Gauss-Legendre integration parameters
% offset               - the offset of the interpolation nodes of the boundary
%                          elements from their geometric limits
% preProcStruc         - auxiliary structure that stores collocation nodes,
%                         domain characteristic length, and optimal place for 
%                         Trefftz eigenexpension (domain barycenter)
%
% Helm_kappa           - if Helm_kappa == 0 => Laplace solver is invoked
%                        if Helm_kappa ~= 0 => Helmholtz solver is invoked
% lambda               - the distance (in normal direction) between 
%                        the boundary and auxiliary (source) points

% Output/returns to MAIN***:
% G,H (and R if necessary) - matrices for dMFS ( Eqs.(15-17) in ref. [2])
%
% BIBLIOGRAPHY
% 1. Patterson C, Sheikh M A - A Regular Boundary Element Method for Fluid Flow
% 2. Borkowski M, Moldovan ID - Direct Boundary Method Toolbox for some
%    elliptic PDEs in FreeHyTE framework, submitted to Advances in 
%    Engineering Software, 2019


if nargin < 8
   % default value of lambda
   lambda = 0.1 ; 
end

if lambda<=0 
   error('Value of ''lambda'' variable cannot be smaller than or equal to 0.') 
end

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% MFS specific computations: 

% shifting collocation nodes (which coincide with interpolation nodes) 
% away from the domain 
MFScollNodes = [preProcStruc.collNodes(:,1) + lambda * preProcStruc.dChL * preProcStruc.collNodes(:,3), ...
                preProcStruc.collNodes(:,2) + lambda * preProcStruc.dChL * preProcStruc.collNodes(:,4) ] ;

%%
% Checking whether any of MFScollNodes is lying inside the domain of
% interest. If this is the case - results can be unreliable.

load('irreg_mesh', 'e', 'ePolygon','polygonNo') ;
load('dBMT_StructDef','p');

% If p is regular rectangular domain there's no need to check anything -
% collocation nodes cannot be moved inside the domain ...
if ~ ( isscalar(p) && p == 0) % ... but otherwise ...
    
    xv = [] ; % xv = zeros(1, size(e,2)+ polygonNo-1 ) ;
    yv = [] ; % yv = zeros(1, size(e,2)+ polygonNo-1 ) ;

    % converting the geometry of the domain to the form that can be passed
    % to Matlab's inpolygon() (-> adding NaN's between each polygon nodes 
    % and 'looping' each polygon by adding the first node as a last node)
    
    for ii = 1:polygonNo
        % TODO: consider doing it with space preallocation

        ind1st = min( find( ePolygon(1,:) == ii ) ) ;

        xv = [xv p(1, e(1, ePolygon(1,:) == ii )) p(1,e(1,ind1st)) NaN];
        yv = [yv p(2, e(1, ePolygon(1,:) == ii )) p(2,e(1,ind1st)) NaN];
    end

    xv(end) = [] ;
    yv(end) = [] ;

    % Check whether "moving by lambda" from the domain boundary do not
    % place _any_ of the MFS collocation nodes inside the polygonal domain
    % defined by the nodes [xv, yv]
    [IN, ON] = inpolygon(MFScollNodes(:,1)', MFScollNodes(:,2)', xv,yv ) ;
    
    
    if any(IN | ON) % if any of the auxiliary node is inside the domain ...
                        % ... or on the polygon boundary ...
        % ... display the warning in the command line and ...
        warning(...
            sprintf(...
         ['Some of the MFS auxiliary nodes happen to be inside the domain. \n' ...
         'The results might me unreliable. \n' ...
         'It seems that lambda paramater = ' num2str(lambda) ' is too large. \n' ...
         'Try to decrease it and run example once again.']))

        % ... plot a figure depicting the distribution of collocation nodes
        % against the domain of interest
        figure, hold on
        plot(xv,yv)
        xq = MFScollNodes(:,1) ;
        yq = MFScollNodes(:,2) ;
        plot(xq(IN),yq(IN),'r+') % points inside 
        plot(xq(~IN),yq(~IN),'go') % points outside
        hold off
        legend('boundary contour', 'disallowed points inside the domain',...
            'points correctly located')
    end
    
end % ENDOF if p ~= 0 % if p is not a regular rectangular domain
       
%%ENDOF MFS specific computations: 
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% no of interpolation/collocation points = dimension of H and G square matrices
[N,~] = size(MFScollNodes) ;
    
% Initialization of the matrices

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Determining some details concerning Robin BC

% There is a need for calculating R vector only if Robin BC 
% du_dn = g(S) + ru
% is imposed at at least one boundary element _and_ function g(S) is
% non-zero there.

RVectorFLAG = 0 ; % flag that informs whether R vector is needed

if any(Edges.type == 'R') % if there is any element with Robin BC prescribed
    try
       gFunValues = cell2mat(BConds.Robin(:,1)) ;
       
       if ~all( ((gFunValues == 0)+isnan(gFunValues))==1 )
           % if there's at least one value of g function
           % that is (nonzero and (not NaN) )
           RVectorFLAG = 1 ;
       end

    catch ME 
       if (strcmp(ME.identifier,'MATLAB:catenate:dimensionMismatch'))
          % if the line 
          % gFunValues = cell2mat(BConds.Robin(:,1)) ;
          % throws an exception, probably the g function prescribed on 
          % one of the boundary elements is of higher degree
          % than zero -> vector R needs to be created
          RVectorFLAG = 1 ;      
       end
       %rethrow(ME)
    end 
end
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% preallocate space for final matrices
H = zeros(N);
G = zeros(N);
if RVectorFLAG
   R = zeros(N,1) ; 
else
   R = [] ;
end


%% Sweeping the edges
for ii=1:length(Edges.nini)
        % LocLoop is a structure where the features of the current
        % element which are directly useful for the calculation of the
        % local blocks of H and G matrices are stored.
        LocEdge =  struct('id',ii,'parametric',Edges.parametric(ii,:),...
            'order',Edges.order(ii),'insert',Edges.insert(ii),...
            'dim',Edges.dim(ii));
        
        % Computing the Gi and Hi matrices of element ii. GHR_Matrix_i is
        % a local function (see below).
        if RVectorFLAG && (~any(isnan(BConds.Robin{ii,1})) && ~all(BConds.Robin{ii,1}==0) ) 
            % if this the element where g function is non-zero
            [Gi, Hi, Ri] = GHR_Matrix_i(LocEdge, abscissa, weight, offset, ...
                MFScollNodes, Helm_kappa, BConds) ;
            % update R vector
            R = R + Ri ;
        else
            % if this is the element where g function does not exist
            [Gi, Hi] = GHR_Matrix_i(LocEdge, abscissa, weight, offset, ...
                MFScollNodes, Helm_kappa) ;    
        end
        
        % Inserting the matrix in the global matrices. The insertion is made
        % at line & column LocEdge.insert(ii).
        G(:,LocEdge.insert:LocEdge.insert+LocEdge.dim-1) = Gi;
        H(:,LocEdge.insert:LocEdge.insert+LocEdge.dim-1) = Hi;
end







end


function [Gi, Hi, vargout] = GHR_Matrix_i(LocEdge, abscissa, weight, offset, ...
            MFScollNodes, Helm_kappa, BConds)       
% GHR_Matrix_i is a local function that computes the G and H (and R?) blocks of the LocEdge element. 
% The boundary elements are mapped to a [-1,1] interval to perform the integrations.
% The integrations are conducted with the use of the following 3D arrays:

%              . . . . .
%             . . . . .
%            /_/_/_/|.
%           /_/_/_/|/   quadrature nodes no (Q)
%          /_/_/_/|/
%         /_/_/_/|/
%  coll   |_|_|_|/  .
%  nodes  |_|_|_|/ . 
%  no     |_|_|_|/.
%   (N)   |_|_|_|/
%         |_|_|_|/
%          .
%          .
%          .
%          shape
%          funs
%           no (M)


% MFScollNodes - [N x 4] array, each row stores [x,y, nx,ny] - global
%             Cartesian coordinates (x,y) of collocation point (shifted outside the boundary) 
%             and the components [nx, ny] of unit normal vector at (x,y)


%% Initialization 
 
    % choose appropriate weighting function depending on the type of
    % problem -> Eqs.(19-20) in ref. [2]
    % fundamental solution and its derivative for Laplace/Poisson 2D problem
    if Helm_kappa == 0
        fh_G 	   = @(r) -log(r)./ (2*pi)  ;       
        fh_dG_dr   = @(r) -1 ./ (2*pi*r) ;
    else
        % fundamental solution and its derivative for Helmholtz 2D problem
        kap = Helm_kappa ;

        fh_G 	 = @(r)  besselk(0, 1i*kap*r)/ (2*pi) ;
        fh_dG_dr = @(r) -(1i*kap/(2*pi)) * ( besselk(1, 1i*kap*r) )  ;
    end

    % no of interpolation/collocation points
    % [N,~] = size(MFScollNodes) ;
    % no of interpolation nodes/shape functions on element
     M = LocEdge.order +1 ;  
    % no of Gauss interpolation nodes
     Q = length(abscissa) ;

    % final size of structures used in integration is [N x M x Q]      

%% Interpolation nodes and shape functions
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    % Generating local coordinates of interpolation nodes and respective 
    % shape functions on element: 
    % interpolation_nodes_distribution = 0 if (LocEdge.order == 0) % constant element
    % interpolation_nodes_distribution is a vector of equidistant points in 
             % the interval [-1+offset,1-offset] for  higher order elements
    % m = 0:LocEdge.order;

    interpolation_nodes_distribution = (LocEdge.order~=0)*...
        linspace(-1+offset,1-offset, LocEdge.order+1) ;
    
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    %%%% Computing shape functions
    % If we assume that all boundary elements are discretized in the same way
    % we can optimize the code by moving this part before "Sweeping the edges" loop.

    [S] = shapeFun1D(abscissa, interpolation_nodes_distribution ) ; % NOTE: line can be moved outside this function and passed as parameter
    % reshaping shape functions to match proper size of 3D grid constructed
    % for integration 
    S3DVec = reshape(S, 1, M, Q ) ;


    %% Generating the geometric data

    % Computing the length of the current edge
    L = sqrt((LocEdge.parametric(3))^2 + (LocEdge.parametric(4))^2);    % NOTE: this can be calculated in preProcessing function and passed here as parameter
    % reshaping abscissa vector to match proper size of 3D itegration grid
    A3DVec = reshape(abscissa, 1,1, Q) ; % [N,~,A]=ndgrid(n,m,abscissa);

    % Getting global coordinates and components of normal vectors for all Gauss points
    XGauss3DVec = LocEdge.parametric(1)  + 0.5 * (A3DVec + 1) * LocEdge.parametric(3);  
    YGauss3DVec = LocEdge.parametric(2)  + 0.5 * (A3DVec + 1) * LocEdge.parametric(4);


    % Computing the distances between Gauss nodes and 'collocation' points    
    X3DVec = bsxfun(@minus, XGauss3DVec, MFScollNodes(:,1) ) ;
    Y3DVec = bsxfun(@minus, YGauss3DVec, MFScollNodes(:,2) ) ;
    R3DVec = hypot(X3DVec, Y3DVec) ;      
    % X3DVec,Y3DVec,R3DVec are [N x 1 x Q] arrays 
    
%% Computing the values of weighting functions for all integration points
    % weighting functions for G matrix
    G = fh_G(R3DVec) ; 
    
    % weighting function for H matrix
    
        % Computing the components of the outward normal in Cartesian directions (H matrix).
        nx = LocEdge.parametric(4) / L;
        ny = -1* LocEdge.parametric(3) / L;
        
        dG_dr = fh_dG_dr(R3DVec) ;
        dr_dn = (X3DVec * nx + Y3DVec * ny) ./ R3DVec ;
        
    dG_dn = dG_dr .* dr_dn ;
        
%% Performing the side integration and updating the field values

    % Preparing weights for integration 
    w3DVec(1,1,:) = weight;

    % constructing integrands
    IntegrandG = bsxfun(@times, G   ,S3DVec) ; 
    IntegrandH = bsxfun(@times,dG_dn,S3DVec) ; 

    Gi = L/2 * sum(bsxfun(@times,IntegrandG,w3DVec),3);
    Hi = L/2 * sum(bsxfun(@times,IntegrandH,w3DVec),3);

    
    if nargout > 2
        % then this is an element with Robin BC -> 
        % there is a need to calculate local component of R vector

        % obtaining the equally spaced points on [-1,1] interval where 
        % g function values are defined and stored in BConds.Robin
        a = linspace(-1,1,length( BConds.Robin{LocEdge.id,1} )) ;

        % obtaining the polynomial that interpolates the values in BConds.Robin
        if (isnan( BConds.Robin{LocEdge.id,1} ))
            error('local:consistencyChk',...
                'No Robin boundary conditions are defined on edge %d. \n',...
                LocEdge.id);
        else
            pol = polyfit(a, BConds.Robin{LocEdge.id,1},...
                length( BConds.Robin{LocEdge.id,1} )-1);
        end

        % computing the values of the interpolation polynomials at
        % quadrature nodes
        g = polyval(pol,abscissa);
        g = reshape(g,1,1,Q) ; 

        IntegrandR = bsxfun(@times,g,G) ; 
        Ri = L/2 * sum(bsxfun(@times,IntegrandR,w3DVec), 3) ;
        
        vargout = Ri ;
    end
        
end
