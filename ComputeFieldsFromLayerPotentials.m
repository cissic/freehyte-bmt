function [U, dU_dx, dU_dy, uBound, qxBound, qyBound, xBound, yBound] = ...
    ComputeFieldsFromLayerPotentials(Edges, uGlob,qGlob, xmesh, ymesh, ...
        abscissa, weight, offset, Helm_kappa) 

% COMPUTEFIELDSFROMLAYERPOTENTIALS computes the final potential and its
% spatial derivatives for the given distribution of the double and single
% layer potentials (uGlob and qGlob) for given points inside the domain.

% ComputeFieldsFromLayerPotentials is called by MAIN***. 
% Input data: 
% Edges       - structure with boundary elements
% uGlob,qGlob - solution at the boundary nodes
% xmesh,ymesh - mesh of points inside the domain for which the solution is
%               to be computed
% abscissa,   - the Gauss-Legendre ... 
% weight        ... integration parameters
% offset               - the offset of the interpolation nodes of the boundary
%                          elements from their geometric limits
% Helm_kappa  - if Helm_kappa == 0 => Laplace problem is being solved
%               if Helm_kappa ~= 0 => Helmholtz problem is being solved                

% Output/returns to MAIN***:
% U, dU_dx, dU_dy - solution U and its partial derivative for the points
%                   inside domain defined by (xmesh,ymesh)
% uBound, qxBound, qyBound - solution U and its partial derivatives for the
%                            points on the boundary
% xBound, yBound           - boundary points for which the boundary is
%                            computed
%
% BIBLIOGRAPHY                
% 1. Borkowski M, Moldovan ID - Direct Boundary Method Toolbox for some
%    elliptic PDEs in FreeHyTE framework, submitted to Advances in 
%    Engineering Software, 2019

if nargin < 9
    Helm_kappa = 0 ;
end

U    = zeros(size(xmesh)) ;
U    = U(:) ;
dUdx = zeros(size(U)) ;
dUdy = zeros(size(U)) ;

% allocating space for the points on BEs in which solution is calculated
% each row corresponds to each BE
xBound = zeros( length(Edges.nini), length(abscissa)) ;
yBound = xBound ;
uBound = xBound ;

qnBound = xBound ; 
qtBound = xBound ; 
qxBound = xBound ; 
qyBound = xBound ; 

[mx, my] = size(xmesh) ;

% fundamental solution and its derivative for Laplace/Poisson 2D problem
if Helm_kappa == 0
    fh_G 	   = @(r) -log(r)./ (2*pi)  ;
    fh_dG_dr   = @(r) -1 ./ (2*pi*r) ;
else
    % fundamental solution and its derivative for Helmholtz 2D problem
    kap = Helm_kappa ; 
    
    fh_G 	 = @(r)  besselk(0, 1i*kap*r)/ (2*pi) ;
    fh_dG_dr = @(r) -(1i*kap/(2*pi)) * ( besselk(1, 1i*kap*r) )  ; 
end

qnn = length(abscissa) ; % quadrature nodes no = no of elements in 3rd dimension in 3D arrays
A2_3DVec = reshape(abscissa, 1,1, length(abscissa) ) ;


%% Sweeping the edges
for ii=1:length(Edges.nini)

%              . . . . .
%             . . . . .
%            /_/_/_/|.
%           /_/_/_/|/   quadrature nodes no
%          /_/_/_/|/
%         /_/_/_/|/
%  coll   |_|_|_|/  .
%  (field)|_|_|_|/ . 
%  points |_|_|_|/.
%  no     |_|_|_|/
%         |_|_|_|/
%          .
%          .
%          .
%          shape
%          funs
%           no

    
    % LocLoop is a structure where the features of the current
    % element which are directly useful for the calculation of the
    % conductivity block are stored.
    LocEdge =  struct('id',ii,'parametric',Edges.parametric(ii,:),...
        'order',Edges.order(ii),'insert',Edges.insert(ii),...
        'dim',Edges.dim(ii));

    % values of double and single layer potential on the element
    uLoc(1,:,1) = uGlob(LocEdge.insert:LocEdge.insert+LocEdge.dim-1) ;
    qLoc(1,:,1) = qGlob(LocEdge.insert:LocEdge.insert+LocEdge.dim-1) ;


    %% Initialization 

    m = 0:LocEdge.order;
    inn = length(m) ;        % interpolation nodes no

    % Computing the length of the current edge
    L = sqrt((LocEdge.parametric(3))^2 + (LocEdge.parametric(4))^2);  

    % Computing the components of the outward normal in the Cartesian
    % directions.
    nx = LocEdge.parametric(4) / L ;
    ny = -1* LocEdge.parametric(3) / L ;

    % Constructing the 3D matrices containing the n x m x abscissa
    % integration grid
    % [N,~,A]=ndgrid( xmesh(:), m, abscissa);
    xmesh = xmesh(:) ;
    ymesh = ymesh(:) ;

    %% Getting the X, Y  for all Gauss points
    % Transforming the edge abscissa (Gaussian node coordinates) into global coordinates.
    X = LocEdge.parametric(1) + 0.5 * (A2_3DVec + 1) * LocEdge.parametric(3);  
    Y = LocEdge.parametric(2) + 0.5 * (A2_3DVec + 1) * LocEdge.parametric(4);

    % storing global (X,Y) coordinates of quadrature nodes in auxiliary variables
    xBound(ii,:) = reshape(X,1,qnn) ; 
    yBound(ii,:) = reshape(Y,1,qnn) ; 


    % distance between Gaussian nodes and the field points as a vector
    distX = bsxfun(@minus, X, xmesh) ; % X - XMESH ;
    distY = bsxfun(@minus, Y, ymesh) ; % Y - YMESH ;        

    % distances between Gaussian nodes and field points
    distR = sqrt( distX.^2 + distY.^2 ) ;        

    %% Generating local coordinates of interpolation nodes on element: 
    % xi = 0 if (LocEdge.order == 0) % constant element
    % xi is vector of equidistant points in the interval [-1+offset,1-offset] for  higher order elements
    interpolation_nodes_distribution = (LocEdge.order~=0)*...
        linspace(-1+offset,1-offset, LocEdge.order+1) ;

    % shape functions and its derivatives
    [S, dS] = shapeFun1D(abscissa, interpolation_nodes_distribution ) ;
    % reshaping S to make it match size of quadrature arrays
    S3D = reshape(S,1,inn,qnn) ;  % S3DNum = reshape(S, 1,LocEdge.order+1, length(abscissa) ) ;

    % G = fh_G(distR) ;   % used only once -> thus calculated inline below

    dG_dr = fh_dG_dr(distR) ;
    inv_distR = 1./distR ;
    dr_dn = (distX * nx + distY * ny) .* inv_distR  ;
    % dG_dn = dG_dr .* dr_dn ;

    % Preparing weights for integration 
    w3D(1,1,:) = weight;

    % Performing the side integration and updating the field values        
    %% Calculations for internal points P = [x_P, y_P]
    
    % implementation of Eq. (18) in ref. [1]:  
    % u(P) = int_Gamma  S*(- u  * dG_dn + q * G) d_Gamma
    GandHLocInte = L/2 * sum(sum( bsxfun( @times, ...
                                            bsxfun(@times,S3D, ...        
                                                          - bsxfun(@times, uLoc , dG_dr .* dr_dn ) ...
                                                          + bsxfun(@times, qLoc ,   fh_G(distR)  ) ...
                                                  ) ...
                                          , w3D), ...
                                  2), ...
                             3) ;

    U = U + GandHLocInte ;        

    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    % d/dx - flux in x direction
    % implementation of Eq. (42) in ref. [1]:  
    % du/dx(P) = int_Gamma  dG_dr * (1/R) * S * (u  * (n_x-2*(x-x_P)*(1/R)*dr_dn) + q * (x-x_P) ) d_Gamma
    GandHxLocInte = L/2 * sum(sum( bsxfun( @times, ...
                                            bsxfun(@times,S3D, ... 
                                                            bsxfun(@times, dG_dr .* inv_distR,  ( ...
                                                                        bsxfun(@times, uLoc, (nx - 2 .* distX .* inv_distR .* dr_dn )) ...
                                                                        - bsxfun(@times, qLoc, distX ) ...
                                                                                  ) ...
                                                                    )...
                                                  ) ...
                                          , w3D), ...
                                  2), ...
                             3) ;

    dUdx = dUdx + GandHxLocInte ;

    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    % d/dy - flux in y direction
    % implementation of Eq. (42) in ref. [1]:  
    % du/dy(P) = int_Gamma  dG_dr * (1/R) * S * (u  * (n_y-2*(y-y_P)*(1/R)*dr_dn) + q * (y-y_P) ) d_Gamma      
    GandHyLocInte = L/2 * sum(sum( bsxfun( @times, ...
                                            bsxfun(@times,S3D, ...
                                                            bsxfun(@times, dG_dr .* inv_distR, ( ...
                                                                        bsxfun(@times, uLoc, (ny - 2 .* distY .* inv_distR .* dr_dn )) ...
                                                                        - bsxfun(@times, qLoc, distY ) ...
                                                                                  ) ...
                                                                   ) ...
                                                  ) ...
                                          , w3D), ...
                                  2), ...
                             3) ;

    dUdy = dUdy + GandHyLocInte ;

    %% Calculations for boundary points        
    % computing solution for boundary points
    uBound(ii,:) = uLoc * S ;                       % Eq.~(35) in ref. [1]
    % computing normal flux on ii-th boundary         
    qnBound(ii,:) = qLoc * S ;                      % Eq.~(36) in ref. [1]

    % computing tangential flux on the boundary
    qtBound(ii,:) = uLoc * dS / (L/2) ;             % Eq.~(37) in ref. [1]

    % Computing flux in x and y direction
    tx = -ny; ty = nx ;                             % Eq.~(38) in ref. [1]
    qxBound(ii,:) = tx * qtBound(ii,:) + nx * qnBound(ii,:) ; % Eq.~(39) in ref. [1]
    qyBound(ii,:) = ty * qtBound(ii,:) + ny * qnBound(ii,:) ; % Eq.~(40) in ref. [1]

end

% vectorizing rows of values for each BElemens of x-y-u-qx-qy-Bound structures
xBound = xBound' ; xBound = xBound(:) ;
yBound = yBound' ; yBound = yBound(:) ;
uBound = uBound' ; uBound = uBound(:) ;

qxBound = qxBound' ; qxBound = qxBound(:) ;
qyBound = qyBound' ; qyBound = qyBound(:) ;


U = reshape(U,mx,my) ;
dU_dx = reshape(dUdx,mx,my) ;
dU_dy = reshape(dUdy,mx,my) ;



end % function [...] = ComputeFieldsFromLayerPotentials(...
